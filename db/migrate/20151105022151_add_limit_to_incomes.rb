class AddLimitToIncomes < ActiveRecord::Migration
  def change
    add_column :incomes, :limit, :integer
  end
end
