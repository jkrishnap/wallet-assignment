class Income < ActiveRecord::Base
  belongs_to :user
  validates :amount, presence: true
  validates :amount, :numericality => { :greater_than_or_equal_to => 0, :message => 'should be positive numerical value' }
  validates :limit, :numericality => { :greater_than_or_equal_to => 0, :message => 'should be positive numerical value' } 
  validates :limit, :numericality => { :less_than_or_equal_to => :amount, :message => 'should be lesser than INCOME value' }
end
